let ehtIcon = document.getElementById("ethI");
let bnbIcon = document.getElementById("bnbI");
let ubxsIcon = document.getElementById("ubxsI");
let btcIcon = document.getElementById("btcI");
let btc = document.getElementById("btc");
let eth = document.getElementById("eth");
let bnb = document.getElementById("bnb");
let ubxs = document.getElementById("ubxs");
let newsOwner = document.getElementById("news-owner");
let newsDetail = document.getElementById("news-detail");
let newsHeadline = document.getElementById("news-headline");
let news = document.getElementById("news");
let liveBroadcast = document.getElementById("live-broadcast");
let hours = document.getElementById("hours");
let subtitle = document.getElementById("subtitle");
let weather = document.getElementById("weather");
let name = document.getElementById("name");
let deg = document.getElementById("deg");
let nameDelaware = document.getElementById("nameDelaware");
let degDelaware = document.getElementById("degDelaware");
let weatherImages = document.getElementById("weatherImage");

setInterval(istanbulWeather, 10000);
setInterval(delawareWeather, 8000);

function istanbulWeather() {
  fetch(
    "https:api.openweathermap.org/data/2.5/weather?q=istanbul&units=metric&appid=ff55584a237b6084b10f4098f8eb5f76"
  )
    .then((response) => response.json())
    .then((istanbulData) => {
      console.log(istanbulData);
      if (istanbulData.weather[0].description === "clear sky") {
        weatherImages.className = "fas fa-cloud-sun fa-2x text-yellow-400";
      }
      if (istanbulData.weather[0].description === "overcast clouds") {
        weatherImages.className = "fas fa-cloud fa-2x text-blue-400";
      }
      if (istanbulData.weather[0].description === "broken clouds") {
        weatherImages.className = "fas fa-cloud-upload-alt fa-2x text-blue-400";
      }
      if (istanbulData.weather[0].description === "sunny") {
        weatherImages.className = "fas fa-sun fa-2x text-yellow-500";
      }
      if (istanbulData.weather[0].description === "rainy") {
        weatherImages.className =
          "fas fa-cloud-showers-heavy fa-2x text-blue-400";
      }

      name.innerHTML = istanbulData.name;
      deg.innerHTML = Math.round(istanbulData.main.temp) + " ºC";
    })
    .catch((error) => {
      console.log("Error:", error);
      name.innerHTML = "veri yok";
    });
}

function delawareWeather() {
  fetch(
    "https:api.openweathermap.org/data/2.5/weather?q=delaware&units=metric&appid=ff55584a237b6084b10f4098f8eb5f76"
  )
    .then((response) => response.json())
    .then((delawareData) => {
      console.log(delawareData);
      if (delawareData.weather[0].description === "clear sky") {
        weatherImages.className = "fas fa-cloud-sun fa-2x text-yellow-400";
      }
      if (delawareData.weather[0].description === "overcast clouds") {
        weatherImages.className = "fas fa-cloud fa-2x text-blue-400";
      }
      if (delawareData.weather[0].description === "broken clouds") {
        weatherImages.className = "fas fa-cloud-upload-alt fa-2x text-blue-400";
      }
      if (delawareData.weather[0].description === "sunny") {
        weatherImages.className = "fas fa-sun fa-2x text-yellow-500";
      }
      if (delawareData.weather[0].description === "rainy") {
        weatherImages.className =
          "fas fa-cloud-showers-heavy fa-2x text-blue-400";
      }
      name.innerHTML = delawareData.name;
      deg.innerHTML = Math.round(delawareData.main.temp) + " ºC";
    })
    .catch((error) => {
      console.log("Error:", error);
      name.innerHTML = "veri yok";
    });
}

istanbulWeather();
delawareWeather();
jsonFile();
function jsonFile() {
  fetch("./icerik.json")
    .then((response) => response.json())
    .then((subtitleData) => {
      subtitle.innerHTML =
        subtitleData[0].title +
        " " +
        "  <i class='fab fa-bitcoin text-white'></i>";

      liveBroadcast.innerHTML = subtitleData[1].title;
      news.innerHTML = subtitleData[2].title;
      newsHeadline.innerHTML = subtitleData[3].title;
      newsDetail.innerHTML = subtitleData[4].title;
      newsOwner.innerHTML = subtitleData[5].title;
    });
}

function time() {
  const today = new Date();
  let hourTime = today.getHours();
  let mimuteTime = today.getMinutes();
  let secondTime = today.getSeconds();
  mimuteTime = checkTime(mimuteTime);
  secondTime = checkTime(secondTime);
  hours.innerHTML = hourTime + ":" + mimuteTime + "";
  setTimeout(time, 1000);
}
time();
function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}
// hour();
// function hour() {
//   const time = new Date();
//   let text = time.toLocaleTimeString();

//   setInterval(hour, 1000);
//   hours.innerHTML = text;
// }

kripto = () => {
  // "https://cors-anywhere.herokuapp.com/https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?CMC_PRO_API_KEY=9b6ac9e8-85ff-45a9-b952-26e19f3929cc&symbol=BTC,ETH,USDT,BNB,UBXS",

  fetch("currency.json", {
    headers: {
      Accept: "aplication/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      btcCoin = parseInt(data.data.BTC.quote.USD.price);
      let oldBtcCoin = localStorage.getItem("btc-coin");
      btc.innerHTML = btcCoin.toLocaleString();

      localStorage.setItem("btc-coin", data.data.BTC.quote.USD.price);
      if (oldBtcCoin > data.data.BTC.quote.USD.price) {
        btcIcon.className = "fas fa-chevron-down text-red-500";
        btc.className = "text-red-500";
      } else {
        btcIcon.className = "fas fa-chevron-up text-green-500";
        btc.className = "text-green-500";
      }

      ethCoin = parseInt(data.data.ETH.quote.USD.price);
      let oldEthCoin = localStorage.getItem("eth-coin");
      eth.innerHTML = ethCoin.toLocaleString();

      localStorage.setItem("eth-coin", data.data.ETH.quote.USD.price);
      if (oldEthCoin > data.data.ETH.quote.USD.price) {
        ehtIcon.className = "fas fa-chevron-down text-red-500 ml-2";
        eth.className = "text-red-500";
      } else {
        ehtIcon.className = "fas fa-chevron-up text-green-500";
        eth.className = "text-green-500";
      }

      bnbCoin = parseInt(data.data.BNB.quote.USD.price);
      console.log(bnbCoin);
      let oldBnbCoin = localStorage.getItem("bnb-coin");
      bnb.innerHTML = bnbCoin.toLocaleString();

      localStorage.setItem("bnb-coin", data.data.BNB.quote.USD.price);
      if (oldBnbCoin > data.data.BNB.quote.USD.price) {
        bnbIcon.className = "fas fa-chevron-down text-red-500";
        bnb.className = "text-red-500";
      } else {
        bnbIcon.className = "fas fa-chevron-up text-green-500";
        bnb.className = "text-green-500";
      }

      ubxsCoin = parseInt(data.data.UBXS.quote.USD.price);
      let oldUbxsCoin = localStorage.getItem("ubxs-coin");
      ubxs.innerHTML = ubxsCoin.toLocaleString();

      localStorage.setItem("ubxs-coin", data.data.UBXS.quote.USD.price);
      if (oldUbxsCoin > data.data.UBXS.quote.USD.price) {
        ubxsIcon.className = "fas fa-chevron-down  text-red-500";
        ubxs.className = "text-red-500";
      } else {
        ubxsIcon.className = "fas fa-chevron-up text-green-500";
        ubxs.className = "text-green-500";
      }
    });
};
kripto();
setInterval(kripto, 10000);
